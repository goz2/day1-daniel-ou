## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective): Today we main studied concept mapping. We learned about the process of creating concept maps and their importance in organizing and visualizing knowledge. We practiced creating concept maps for various subjects and topics.

- R (Reflective): Engaged.

- I (Interpretive): Concept mapping is a valuable tool for understanding and connecting information. It allows us to see the relationships between different concepts and how they fit into a larger framework. The most meaningful aspect of this activity was realizing the power of visual representation in enhancing learning and comprehension.

- D (Decisional): I would like to apply what I have learned about concept mapping in my studies and work. I will start incorporating concept maps into my note-taking process to better organize and retain information. I believe this will help me make better connections between ideas and improve my overall understanding of complex topics.