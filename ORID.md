## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective):  I learned about React's useEffect hook and its usage in managing side effects in React components.The roles involved in a simulation project, which helped me gain an understanding of the different responsibilities and contributions of team members, such as simulation developers, domain experts, project managers, and stakeholders.How to write user stories, user journeys, and user maps. These techniques allowed me to better empathize with users, identify their needs, and map out their interactions with a product or service.

- R (Reflective): Enlightened.

- I (Interpretive): Today's class was very insightful. I see how all the topics are interconnected and can be applied together in real-world software development projects. The most meaningful aspect for me was learning about user stories and journeys. Understanding the user's perspective and needs is crucial for building user-centric products.

- D (Decisional): I am excited to apply what I have learned today in my future projects. Specifically, I want to focus on creating well-defined user stories and journeys to ensure that the end product meets the users' expectations and solves their problems effectively.